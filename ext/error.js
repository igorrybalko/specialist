function setError(num, msg){
	msg = msg || 'Что-то случилось';
	var err = new Error(msg);
	err.status = num || 404;
	return err;
}

exports.setError = setError;