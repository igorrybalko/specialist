var User = require('../schema/user').User,
	e = require('../ext/error'),
	helper = require('../ext/helper');

module.exports = function(app){

	app.get('/', function(req, res, next){

		if(req.session.user){

			User.findById(req.session.user, function(err, user){
				res.render('index', {title: "Наш сайт", name: user.name, guest: 0});
			});

		}else{
			res.render('index', {title: "Наш сайт", name: "Гость", guest: 1});
		}
	
	});

	app.get('/users/:name', function(req, res, next){
		
		User.findOne({name: req.params.name}, function(err, user){
			if(err) next(err);
			if(user === null){
				next(e.setError(404, 'User not found'));
			}
			var guest = 1;
			if(req.session.user){
				guest = 0;
			}
			res.render('user', { 
				title: 'Пользователь',
				name: user.name, 
				create: helper.getDate(user.create),
				guest: guest
			})

		});
	});

	app.get('/users', function(req, res, next){
		
		User.find({}, function(err, users){
			if(err) next(err);

			var prepareUsers = [],
				guest = 1;

			if(req.session.user){
				guest = 0;
			}

			users.forEach(function(user){
				prepareUsers.push({
					name: user.name,
					create: helper.getDate(user.create)
				})
			});

			res.render('users', {
				users: prepareUsers, 
				title: 'Пользователи', 
				guest: guest
			});
		});

	});

	app.get('/chat', function(req, res, next){
		if(req.session.user){

			User.findById(req.session.user, function(err, user){
				res.render('chat', {title: "Наш сайт", name: user.name, guest: 0});
			});

		}else{
			res.render('chat', {title: "Наш сайт", name: "Гость", guest: 1});
		}
	});

	app.get('/login', function(req, res, next){
		if(req.session.user){
			res.redirect('/');
			res.end();
		}
		res.render('login', {title: "Login", guest: 1});
	});

	app.post('/login', function(req, res, next){
	
		var login = req.body.login;
		var pass = req.body.password;

		User.findOne({name: login}, function(err, curUser){

			if(err) next(err);

			if(curUser){
				if(curUser.checkPassword(pass)){
					req.session.user = curUser._id;
					res.redirect('/users/' + login);
					res.end();
				}else{
					next(e.setError(401));
				}
			}else{
				next(e.setError(401));
			}

		});

	});

	app.get('/register', function(req, res, next){
		if(req.session.user){
			res.redirect('/');
			res.end();
		}
		res.render('register', {title: "Register", guest: 1});
	});

	app.post('/register', function(req, res, next){

		var password = req.body.password;

		if(password.trim().length > 4){
			if(password === req.body['password-confirm']){

				var user = new User({
					name: req.body.login,
					password: password
				});

				user.save(function(err){
				
					if(err) console.log('error registration');
					res.redirect('/users/' + req.body.login);
					res.end();
				
				});
			}
		}
	});

	app.get('/logout', function(req, res, next){

		req.session.destroy(function(err) {
		  if(err) next(err);
		});

		res.redirect('/');
		res.end();

	});

}