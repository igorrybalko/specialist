var express = require('express'),
	app = express(),
	path = require('path'),
	bodyParser = require('body-parser'),
	cookieParser = require('cookie-parser'),
	session = require('express-session'),
	conf = require('./config'),
	log = require('./ext/log'),
	listenApp = app.listen(conf.get('port'));

require('./ext/chat')(listenApp);

app.engine('ejs', require('ejs-locals'));
app.set('views', path.join(__dirname, conf.get('app-view')));
app.set('view engine', conf.get('app-engine'));
app.use(express.static(path.join(__dirname, conf.get('app-static'))));
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.use(cookieParser());
app.use(session({
	secret: conf.get('session:secret'),
	key: conf.get('session:key'),
	cookie: conf.get('session:cookie'),
	resave: true,
    saveUninitialized: true
}));

require('./routes')(app);

app.use(function(req, res, next){
	var err = new Error('Not found');
	err.status = 404;
	next(err);
});

if('development' == app.get('env')){

	app.use(function(err, req, res, next){
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err,
			title: 'Ошибка',
			guest: 1
		});
	});
}

app.use(function(err, req, res, next){
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {},
		title: 'Ошибка',
		guest: 1
	});
});